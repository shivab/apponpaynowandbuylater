﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace AppOnPayNowAndBuyLater.Helpers
{
    public static class DropDownHelpers
    {
        public static List<SelectListItem> GetCoutries()
        {
            var coutriesList = new List<SelectListItem>()
            {
                new SelectListItem
                {
                    Text = "United States",
                    Value = "United States"
                },
                new SelectListItem
                {
                    Text = "United Kingdom",
                    Value = "United Kingdom"
                },
                new SelectListItem
                {
                    Text = "South Africa",
                    Value = "South Africa"
                },
                new SelectListItem
                {
                    Text = "India",
                    Value = "India"
                }
            };
            return coutriesList;
        }
        public static List<SelectListItem> GetPaymentTypes()
        {
            var paymentsTypesList = new List<SelectListItem>()
            {
                new SelectListItem
                {
                    Text = "Klarna Payments",
                    Value = "Klarna Payments"
                }
            };
            return paymentsTypesList;
        }
    }
}